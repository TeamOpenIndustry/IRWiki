# Immersive Railroading Wiki

This site uses [VuePress](https://vuepress.vuejs.org/) right now.
Also see [VuePress Tutorial](https://vuepressbook.com/).

If you find anything wrong, please open an [issue](https://gitlab.com/ImmersiveRailroading/immersiverailroading.gitlab.io/issues) or fix it yourself and submit a [Merge Request](https://gitlab.com/ImmersiveRailroading/immersiverailroading.gitlab.io/merge_requests).

## Guidelines

1. Each page must stand alone. Assume that the user has not seen anything other page.
   * Link on the first instance of an item, tool, machine, etc.
   * Point to other guides
   * Have navigation on the title if the page is 2 or more deep.
1. Items have the following sections
   1. An introduction
   1. Crafting Instructions
   1. Usage Instructions
1. Multi-block structures have the following sections
   1. An introduction
   1. Crafting with IE
   1. Crafting without IE
   1. Usage
1. Spell check
1. Have someone else proof-read what you add
   * en_US: Any Developer
   * de_DE: ?
   * ja_JP: ?
   * ru_RU: ?
   * zh_CN: ?
