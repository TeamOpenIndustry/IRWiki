Please fill out the following. Your pack should be based on our starter pack for the best compatibility.  
The name of the request should be `[Resource Pack Request] Your Pack's Name`  
Please see the [resource pack developer page](https://teamopenindustry.gitlab.io/IRWiki) on how to create a MR properly.
Please remove this line and the above lines before submitting.

**Name of the Resource pack:** Your Pack's Name

**Preferred project URL** (no spaces): Your_Pack

All other information is included in this merge request's changes to reference/models/resourcepacks.md.

