# Featured World Request

World requests should only be for worlds that include Immersive Railroading.
Please upload to a world sharing site, such as Planet Minecraft
or a file sharing site like Dropbox or Google Drive.
It is suggested that you attach a screenshot or image.
Please remove this first part before submission.

## World URL

https://example.com/world

## World Description

This is your description

## Mod List

In order for others to play your map, we need to know the mods used to make the map.
You can either list it here or attach a directory listing as a txt file.

* Immersive Railroading
* Other Mod
