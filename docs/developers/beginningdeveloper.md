---
title: The Beginner Developer
---

# Dev Guide: Beginning Developing with IR

## Requirements

1. Know how to play Minecraft
1. Terminal experience
1. Know how to program Java and have the JDK installed (1.8)
1. Know how to use Git

## Fork it

The first thing to do if you want to start contributing to this project is to fork it.

After that, it might be good to create a new branch on your forked project with a good short name describing what you are adding or fixing.

## Setup your dev environment

You can use any major OS - Windows, Mac, and Linux - since this Java.

It is recommended that you use either Eclipse or Jet Brain's IntelliJ.  
Visual Studio Code can also be used, but you will need to use the terminal to build and test.

Next, clone your forked git repo.
While you are in the terminal, it would probably be good to add the upstream branch.
`git remote add upstream <url of IR Repo>`
You will use this to make sure your code is up to date before you do a merge request.

## Setup gradle environment

Next, get all the decompiled MC sources for your workspace: `./gradlew setupDecompWorkspace`

Then, setup the environment for your specific IDE:  
For IntelliJ, run `./gradlew idea`  
For Eclipse, run `./gradlew eclipse`

## Get the other mods

You will need a copy of TrackAPI and Immersive Engineering (decomp).
Find these and save them to the root of the project.

## Testing Commands

`./gradlew runClient`  - Runs minecraft as a generic user with IR. You will need to add mods to run/mods.
`./gradlew runServer`
`./gradlew build` - Builds a non-dev version to add and run like normal.
