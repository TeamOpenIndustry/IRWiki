---
title: About the Developers
---

# About the Developers

There is not much here here...yet.
Just want to show off the excellent developers involved with this project.

Christian Mesh (cam72cam) - Lead Developer
thelegend5550  
WrongWeekend  
DennisTheCan  
Peyton Smith (Thomas4peyton)  
Larky2k  
FriedrichLP  
TartaricAcid  
gaojie429  
StuffedCroc  
Fleegle411  
Kai Mills (DarkRaider, km00700)  
EweLoHD  
Benjamin Plain (bplainia, blackhawkrider) - GitLab, math, logic, website  
Jacob Hayes (tigerbird1)  
Mason Besmer (HealthCareUSA)  
