---
title: Train Physics
---

# Physics

This page is about the physics of trains, both real and within IR.

## Overall Physics - The Accumulator

The accumulator is responsible for keeping track of the velocity of rolling stock.

Its class is `cam72cam.immersiverailroading.physics.PhysicsAccumulator`.

For each entity, the accumulator is ran each tick.

::: tip Suggestion
Turn off when the train is not moving and the air brake is 100%.  
Rolling stock should have brake 100% when not connected.
This brake should be able to be turned off.
:::

All forces are in Newtons as a double.

| I/O    | Name            | Applied to    | Direction         | Letter         | Unit      |
|--------|-----------------|---------------|-------------------|----------------|-----------|
| input  | Track Grade     | Stock on hill | Downhill          | &Theta;        | Radians   |
| input  | Mass to move    | Whole Train   |                   | $m_{Total}$    | kilograms |
| input  | Tractive Effort | Locomotives   | Both              | $F_{Tractive}$ | Newtons   |
| input  | Loco. Braking   | Locomotives   | Opposite movement | $F_{LB}$       | Newtons   |
| input  | Air Brake       | Whole Train   | Opposite movement | $F_{AB}$       | Newtons   |
| input  | Track Adhesion  | Whole Train   |                   | &mu;           | unitless  |
| output | Velocity        | Whole Train   |                   | V              | m/s       |

Please note that N/Kg = m/s^2, the unit of acceleration in MC.
To convert this to velocity, it must be multiplied by a unit of time.
Since the accumulator runs each tick, and a tick is 0.05s, the velocity equation would be `@V = \frac{\sum F}{m_Total}@`

Tractive effort is a function of the throttle and the maximum force of all the locomotives in the train.

## Steam Locomotives

## Diesel-Electric Locomotives

## Electric Locomotives

Even though electric locomotives have not been implemented, they are very similar to Diesel-Electric locomotives,
as both use electric motors to move and brake.

## Rail Cars

## The Throttle

Currently the throttle is a double.
Naturally, positive moves forward, negative moves in reverse.

There is an open issue to change this so that there is a reverser instead.

## Air (Pneumatic) Brakes

The standard way to brake train cars is the opposite from what some would think (Air pressure directly goes from locomotive to cars).
Air pressure is used to open brakes.
This is an automatic safety system originally made by Westinghouse.
Each car has its own pressure tank that is filled during normal opperations.

In IR, air brakes are a single variable that is applied to the accumulator.
This is similar to modern brakes which use remote movements

## Dynamic or Regenerative Brakes

Locomotives that use electric motors are capable of also using the same motors for braking.

This produces a braking force in the locomotive only.

Currently, there is no implementation in IR.

### DC Motors

DC Motors are not good at doing Dynamic braking at low speeds.

Low speed: Current limited. force-speed graph is a bunch of spikes.  
Medium Speed: Voltage limited. Graph is inverse exponential.
High speed: Commutator limited. Graph is again exponential.

DC Motors are not capable of doing regenerative braking, as they produce a lower voltage than what they are driven at.

### AC Motors

The force-speed curve for AC motors is much different from DC motors.
At low speeds, the force created is speed independent - the SD90 is independent up to 30km/h at a braking force of 525kN. (See reference 4)
After 30km/h, the braking force steadily reduces logarithmically until it levels off at 100kN, which is about the same amount of tractive effort at high speed.
If the graph was logarithmic, the line would probably be a straight line - for those engineers out there.
Do note that the tractive effort is usually higher initially (825kN until 20km/h), as the motors cannot be ran at full power while braking.

AC motors are also the only type that is able to do regenerative braking.
Instead of wasting the energy through resistive loads, the energy is either stored or returned to the grid.

## Sanding

This mechanic will add a friction coefficient (&mu;).

Sand adds 0.1 to &mu;
Rain subtracts 0.1 from &mu;
Normal &mu; is 0.20 (Lowest speed) to 0.25 (highest speed). The curve is exponential.

If different materials for bogies/wheels or rails are introduced, this coefficient will need to be changed.

Exceeding friction will cause wheels to slip (too much tractive effort) or lock (too much braking). This will need an animation.

## Derailment

Derailment is currently a suggestion.

There are a number of situations where a train can derail.

1. Going too fast around a tight curve or turnout. Train tips due to centrifugal force ('@V^2 / r@').
1. Hitting the end of a track.
1. Hitting another train

## Damage

Damage is currently a suggestion.

### Trains

_Suggestion:_ Train entities should be able to get damage from:

* Derailment
* Weapons
* Falling Blocks (Very slight damage? Maybe implement scratches)
* Wheel Lockup or excessive braking
* Overheating

Damage will be added to individual components so that it is repairable.

Each failed component will lead to different results.
For example, if a boiler fails and is pressurized, then it will explode.
If a train derails, it will damage wheels and couplers.  
If the mode is set to hard and emergency brake is set, couplers have a chance to receive damage from switching directions. Chances are higher in the forward part of the train.

### Players and Mobs

Players and mobs are able to receive damage from moving trains. _Beware of moving trains_ just like in real life.

## References

_1_ [Horspower and Tractive Effort](http://evilgeniustech.com/idiotsGuideToRailroadPhysics/HorsepowerAndTractiveEffort/)

_2_ [Locomotive power calculations](http://www.republiclocomotive.com/locomotive-power-calculations.html)

_3_ [Air Brake Principals](http://www.wplives.org/forms_and_documents/Air_Brake_Principles.pdf)

_4_ [Dynamic Braking Control for Accurate Train Braking Distance Estimation under Different Operating Conditions](https://vtechworks.lib.vt.edu/bitstream/handle/10919/19322/Ahmad_HA_D_2013.pdf;sequence=1) by Husain Abdulrahman Ahmad
