---
title: Resource Pack Developers
---

# Resource Pack Developers

We greatly appreciate all those that create amazing models for Immersive Railroading.

This page is dedicated to helping those that make resource packs.
If there is something we need to add, mention it on discord.

## Rules

1. Respect the ownership of Resource Pack developers. If you have write access to their pack, always ask first before doing any changes, and only do changes through branches and MR's.
2. Always include the license file in your pack's repo.
3. Please use the standard starter pack as a base.

## New Packs

Thank you for your interest in wanting to make a pack for Immersive Railroading.

To get started with a new pack, please clone the [starter resource pack](https://gitlab.com/TeamOpenIndustry/ir-resource-packs/ir-starter-pack).
This pack includes all the code to get you started with the standard IR Resource Pack GitLab pipeline.

Once you are ready to publish, do the following so that you can get on the list and get your own project in the IR resource packs section.

1. [Fork this site](https://gitlab.com/TeamOpenIndustry/IRWiki/-/fork/new)
1. Modify /docs/reference/models/resourcepacks.md, 
   - Packs are in alphabetical order
   - Look at the other packs - there is a standard format
1. Create a new Merge request on this [site's project](https://gitlab.com/TeamOpenIndustry/IRWiki). Please use the Resource Pack template.
1. Once your project is created, change the origin of your local git repo to your project.  
   The command for this should be similar to `git remote set-url origin https://gitlab.com/TeamOpenIndustry/ir-resource-packs/yourpack.git`.  
   Visit your project to get the proper URL. After this, push to your project for the first time.
1. If it builds, a developer will take a look to make sure everything is OK and approve the merge request.

## Research

Please do your research on the models so you can include some interesting facts in the wiki.

Here are a few good sources of info.

- [Locomotive Wiki](https://locomotive.fandom.com/wiki/)
- [American Rails](https://www.american-rails.com/)
- Wikipedia