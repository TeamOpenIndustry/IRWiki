---
title: Developer Section
---

# Developer Section

This section is for developers or for those wanting to know how IR works.

## Table of Contents

* [About the Developers](about.md)
* [Physics](physics.md)
* Rendering
* Redstone
* Entities
* [Understanding the ModCore](modcore.md)
* [Guide for Beginners](beginningdeveloper.md)
* [Resource Pack Developer Help](resourcedevelopers.md)

::: danger
Very little of this has been proof-read by experienced people.
:::
