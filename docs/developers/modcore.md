---
title: Modcore
---

# Modcore

In order to make IR more compatible across versions, the code base is separated into two parts - modcore and IR.

## Layers

### Application

Minecraft Java (1.7.2, 1.12.2, 1.14.1, etc)

---

### Mod Loader API's

Forge or Fabric

---

### Abstract Libraries

Modcore (Version Specific), TrackAPI,

---

### Linking Libraries

Modcoremod - links modcore to IR

---

### Mods

Immersive Railroading and Immersive Engineering
