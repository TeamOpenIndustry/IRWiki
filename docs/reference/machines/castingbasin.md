---
title: Casting Basin
---

# [Machines](./) - Casting Basin

<ModelViewer type="multiblock" model-name="casting_machine"/>

The Basin creates different parts for rolling stock, specifically wheels and frames, as well as the Rail Cast for use with the [Track Roller](trackroller.md). The Gauge selector changes between gauges.

## Building with Immersive Engineering

It is built from:
* 114 x Stone Bricks
*  15 x Heavy Engineering Block
* 126 x Sand
*  86 x Blast Brick
*  63 x Steel Scaffolding
*   1 x Block of Steel

![The Basin before assembly](/img/castingbasin-iebeforecomplete.png)

## Building without Immersive Engineering

> Need Backup blueprint and item counts

## Usage

Power is input through the top of the machine, on the color-coded yellow/orange block.

To create a selected part, drop Steel Blocks or Steel Ingots into the Basin and make sure it has power.
The part will be created in short order - right-click on the Basin to retrieve the part.

Parts made in the casting basin can be recycled by dropping them back into the basin.
