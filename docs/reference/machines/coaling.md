---
title: Coaling Station
---

# [Machines](./) - Coaling Station

::: warning
Proposed - Not Implemented
:::

This machine will fill up steam locomotives and their tenders with coal.
