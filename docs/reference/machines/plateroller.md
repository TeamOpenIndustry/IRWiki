---
title: Plate Rolling Machine
---

# [Machines](./) - Plate Rolling Machine

<ModelViewer type="multiblock" model-name="plate_rolling_machine"/>

The Plate Rolling Machine creates Steel Plates and Boiler Plates from Steel Blocks, which are used in the crafting of Locomotives.

## Building with Immersive Engineering

You need

* 41 x Heavy Engineering Block
* 90 x Steel Scaffolding
* 88 x Light Engineering Block

> Need Multi-block Image

## Building without Immersive Engineering

> Need Backup blueprint and item counts

## Usage

Power is input on the uppermost top block, and can be from any RF/IF-compatible power transmission method such as IE wires, TE conduits, etc.

Steel Blocks are input by right-clicking the machine with the blocks in hand, and Steel Blocks can be placed while a plate is being made, which makes the process a bit more efficient.

Right-click on the machine to view the GUI and select the type of plate to create.
The Gauge selector determines the gauge of the resulting plates.
The plate type selector changes between different types of plate each locomotive recipe requires.
The Boiler Plate is used in the creation of steam-powered engines after processing in the [Boiler Roller](boilerroller.md).
