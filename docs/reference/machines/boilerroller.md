---
title: Boiler Roller
---

# [Machines](./) - Boiler Roller

<ModelViewer type="multiblock" model-name="boiler_rolling_machine"/>

The Boiler Roller turns Boiler Plates created in the [Plate Rolling Machine](plateroller.md) into the needed boiler pieces for a given steam locomotive.

## Building with Immersive Engineering

You need
* 24 x Stone Slab
* 4 x Heavy Engineering Block
* 8 x Light Engineering Block

![Multiblock Structure](/img/boilerroller-iestructure.png)

## Building without Immersive Engineering

If Immersive Engineering is not installed, the Boiler Roller will be built from:
* 24 x Stone Slab
* 12 x Iron Block

![Backup Multiblock Structure](/img/boilerroller-vanstructure.png)

## Usage
Power is input through the top of the machine, on the color-coded yellow/orange block.

The roller creates the appropriate part automatically - just have a boilerplate in hand (made in the [Plate Roller](plateroller.md)) and right-click the machine.
After a few moments, the flat plate will be rolled - right-click the machine with an empty hand to pick it up.