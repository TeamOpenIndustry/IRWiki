---
title: Track Roller
---

# [Machines](./) - Track Roller

<ModelViewer type="multiblock" model-name="rail_machine"/>

The Track Roller is used to create track pieces for use with Track Blueprints.

The Track Roller requires a clear, level space that is 30 blocks deep, 2 blocks wide, and 3 blocks tall.
When placed, the short side will be facing the player, and offset 1 block to the right.
After placing, 

## Building with Immersive Engineering

You need

* 44 x Steel Scaffolding
* 16 x Heavy Engineering Block
* 32 x Light Engineering Block

![Track Roller Multiblock Structure](/img/trackroller-iestructure.png)

## Building without Immersive Engineering

If Immersive Engineering is not installed, the Track Roller will be built from:

* 44 x Iron Bars
* 48 x Iron Block

![Track Roller Multiblock Structure without IE](/img/trackroller-vanstructure.png)

To complete, right-click with a Large Wrench anywhere on the arrangement of blocks to assemble the machine.

## Usage

Power is input through the top of the machine, on the color-coded yellow/orange block.

The Track Roller requires power to operate, which can be input at the top of the multiblock via any RF/IF-compatible method of power transfer.
To create track sections, make a Rail Casting in the [Casting Basin](castingbasin.md) and right-click the Track Roller with the casting.
After the animation completes, right-click the multiblock to receive 10 track segments in a given gauge.

These Track Segments are used with the [Track Blueprint](/reference/items/trackblueprint.md) to lay down sections of track.
