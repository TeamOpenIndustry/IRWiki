---
title: Steam Hammer
---

# [Machines](./) - Steam Hammer

<ModelViewer type="multiblock" model-name="steam_hammer"/>

The Steam Hammer is used to process some parts before they are placed onto a Frame.
It is the smallest machine in Immersive Railroading, requiring a mere 5 blocks in width, 1 in depth, and 6 in height.

## Building with Immersive Engineering

* 3 x Heavy Engineering Block
* 1 x Block of Steel
* 1 x Piston
* 10 x Light Engineering Block

![Steam Hammer IE Multiblock](/img/steamhammer-iestructure.png)

## Building without Immersive Engineering

If Immersive Engineering is not installed, the Steam Hammer will be built from:

* 1 x Piston
* 14 x Iron Block

![ Steam Hammer Multiblock without IE](/img/steamhammer-vanstructure.png)

## Usage

Operating the Steam Hammer requires power, which is input through the top block.
To operate the hammer, right-click the central block to open the GUI.
Place the item that should be processed in the left slot, and after a few moments it will move to the right slot, which means it is now ready for use.
