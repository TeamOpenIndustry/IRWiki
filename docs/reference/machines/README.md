---
title: Multi-block Machines
---

# Machines

This is a list of the multi-block machines in IR.

* [Track Roller](trackroller.md)
* [Casting Basin](castingbasin.md)
* [Steam Hammer](steamhammer.md)
* [Plate Roller](plateroller.md)
* [Boiler Roller](boilerroller.md)

All structures are in the [Blueprint Book](/reference/tools/blueprintbook.md).
It is suggested to use the Blueprint Book to quickly place all blocks with a single click.

Structures are completed by right-clicking anywhere with a [Large Wrench](/reference/tools/largewrench.md).

## Suggested structures

* Coaling Tower
* Water Tower
* Fueling Pipes
* Freight Grate
* Freight Loader
* Signals
* Crossing Gates
