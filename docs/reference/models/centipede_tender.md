---
title: centipede_tender
---

# [Models](./) - Centipede Tender

<ModelViewer type="tender" model-name="centipede_tender"/>

## History/Overview

In the game, the Centipede Tender pairs with the [Big Boy](big_boy) locomotive.

Someone else knows more about this.

## Building

1. Frame
