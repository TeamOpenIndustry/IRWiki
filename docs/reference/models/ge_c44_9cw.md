---
title: GE C44-9CW
---

# [Models](./) - GE C44-9CW (Dash 9)

<ModelViewer type="diesel" model-name="ge_c44_9cw"/>

## History/Overview

<WheelConfiguration>C-C</WheelConfiguration>

## Building

1. Frame
