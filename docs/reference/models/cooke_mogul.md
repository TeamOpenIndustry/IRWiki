---
title: Cooke Mogul
---

# [Models](./) - Cooke Mogul

<ModelViewer type="steam" model-name="cooke_mogul" gauge="1"/>

## Overview

[Tender](cooke_tender)

Wheel Configuration: <WheelConfiguration>2-6-0</WheelConfiguration>

Beginning of Manufacture: 1884

## Building

1. Frame
