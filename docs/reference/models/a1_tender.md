---
title: A1 Peppercorn Tender
---

# [Models](./) - A1 Peppercorn Tender

<ModelViewer type="tender" model-name="a1_tender"/>

## History/Overview

This tender pairs with the [A1 Peppercorn](a1_peppercorn).

Someone else knows more about this.

## Building

1. Frame
