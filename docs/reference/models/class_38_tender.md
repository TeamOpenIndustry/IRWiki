---
title: Class 38 Tender
---

# [Models](./) - Class 38 Tender

<ModelViewer type="tender" model-name="class_38_tender"/>

## History/Overview

This tender pairs with the [Class 38](class_38) locomotive.

Someone else knows more about this.

## Building

1. Frame
