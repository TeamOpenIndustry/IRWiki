---
title: K4 Pacific
---

# [Models](./) - K4 Pacific

<ModelViewer type="steam" model-name="k4_pacific"/>

## Overview

[Tender](k4_tender)

<WheelConfiguration>4-6-2</WheelConfiguration>

Made by ALCO for the Pennsylvania Railway

## Building

1. Frame
