---
title: Iron Duke
---

# [Models](./) - Great Western Railway Iron Duke

<ModelViewer type="steam" model-name="iron_duke" gauge="2.17"/>

## Overview

<WheelConfiguration>4-2-2</WheelConfiguration>

## Building

1. Frame
