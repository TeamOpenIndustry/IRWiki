---
title: Models Reference
---

The following models are built into Immersive Railroading.
Additional models can be added through [Resource packs](./resourcepacks.md).

## Steam Locomotives

- [A1 Peppercorn](a1_peppercorn) and [Tender](a1_tender)
- [A5 Switcher](a5_switcher) and [Tender](a5_tender)
- [Big Boy](big_boy) and [Tender](centipede_tender)
- [BR01](br01) and [Tender](br01_tender)
- [Class 38](class_38) and [Tender](class_38_tender)
- [Cooke Mogul](cooke_mogul) and [Tender](cooke_tender)
- [E6 Atlantic](e6_atlantic) and [Tender](e6_tender)
- [Firefly](firefly)
- [Iron Duke](iron_duke)
- [K36](k36) and [Tender](k36_tender)
- [K4 Pacific](k4_pacific) and [Tender](k4_tender)
- [Rodgers 460](rodgers_460) and [Tender](rodgers_460_tender)
- [Skookum](skookum) and [Tender](skookum_tender)

## Diesel Locomotives

- [EMD SD-40-2](emd_sd40-2)
- [Alco RS1](alco_rs1)
- [EMD SW1500](emd_sw1500)
- [GE 40-Ton Boxcab](ge_40_ton_boxcab)
- [GE B40 8](ge_b40_8)
- [GE B40 8W](ge_b40_8w)
- [GE C44 9CW](ge_c44_9cw)

## Other Locomotives

- [Handcart](handcart_1)

## Rolling Stock

### Freight

- [SLSF 70t Hopper](70t_hopper_slsf)
- [ATTX Flatcar](attx_flatcar_1)
- [Boxcar X26](boxcar_x26)
- [DRGW 1000 Class Gondola](drgw_1000class_gondola)
- [DRGW 3000 Class Boxcar](drgw_3000class_boxcar)
- [DRGW 5500 Class Stockcar](drgw_5500class_stockcar)
- [DRGW Rail and Tie Outfit](drgw_rail_and_tie_outfit)
- [DW Gondola](dw_gondola)
- [PRR Flatcar](prr_flatcar_1)
- [Russell Snow Plow](russell_snow_plow)
- [USRA Boxcar Class 40'](usra_boxcar_classrr40)
- [USRA Hopper 55t](usra_hopper_55t)
- [X26 Boxcar](boxcarx26)

### Caboose

- [N5C Cabin Car](n5c_cabin_car)
- [Waycar](waycar)

### Tankers

- [KAMX](kamx_t)
- [Slag Car](slag_car_1)
- [Tank US2](tank_us2)
