---
title: Rodgers 460
---

# [Models](./) - Rodgers 4-6-0 (Ten Wheeler)

<ModelViewer type="steam" model-name="rodgers_460"/>

## Overview

[Tender](rodgers_460_tender)

<WheelConfiguration>4-6-0</WheelConfiguration>

## Trivia

The Sierra No. 3 is one of the most photographed locomotives ever.

## Building

1. Frame
