---
title: Big Boy
---

# [Models](./) - Union Pacific Big Boy

<ModelViewer type="steam" model-name="big_boy"/>

## History/Overview

The Big Boy and several other locomotives used the [Centipede Tender](centipede_tender).

<WheelConfiguration>4-8-8-4</WheelConfiguration> (A unique configuration)

## Building

1. Frame
