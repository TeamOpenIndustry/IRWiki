---
title: Firefly
---

# [Models](./) - Great Western Railway Firefly

<ModelViewer type="steam" model-name="firefly" gauge="2.17"/>

## Overview

<WheelConfiguration>2-2-2</WheelConfiguration>

Original Gauge: Brunel

## Building

1. Frame
