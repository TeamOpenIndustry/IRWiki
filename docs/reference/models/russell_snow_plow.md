---
title: Russell Snow Plow
---

# [Models](./) - Russell Snow Plow

<ModelViewer type="freight" model-name="russell_snow_plow"/>

## Overview

A wedge plow. Put it on the front of the train to look cool.

Snow does not accumulate on the tracks...yet.

## Building

1. Frame
