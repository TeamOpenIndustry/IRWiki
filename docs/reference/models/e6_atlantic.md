---
title: E6 Atlantic
---

# [Models](./) - Pennsylvania Railroad E6 Atlantic

<ModelViewer type="steam" model-name="e6_atlantic"/>

## Overview

[Tender](e6_tender)

<WheelConfiguration>4-4-2</WheelConfiguration>

## Building

1. Frame
