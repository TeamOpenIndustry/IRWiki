---
title: Rodgers 460 Tender
---

# [Models](./) - Rodgers 460 Tender

<ModelViewer type="tender" model-name="rodgers_460_tender"/>

## Overview

This tender pairs with the [Rodgers 460](rodgers_460).


## Building

1. Frame
