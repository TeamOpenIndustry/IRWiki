---
title: Class 38
---

# [Models](./) - New South Wales C38

<ModelViewer type="steam" model-name="class_38"/>

## Overview

[Tender](class_38_tender)

<WheelConfiguration>4-6-2</WheelConfiguration>

## Building

1. Frame
