---
title: A1 Peppercorn
---

# [Models](./) - A1 Peppercorn

<ModelViewer model-name="a1_peppercorn" type="steam" />

## Overview

[Tender](a1_tender)

<WheelConfiguration>4-6-2</WheelConfiguration>

## Building

1. Frame
1. Steam Chest
1. Bogey Front wheel, Wheel Driver, Bogey Front, Frame Wheel
1. 3 of each (all cast and then steam hammered)
   * Union Link
   * Piston Rod
   * Combination Lever
   * Side Rod
   * Eccentric Rod
   * Main Rod
   * Expansion Link
   * Radius Bar
   * Eccentric Crank
1. 3 Boiler Segments (Plate roller, Boiler Roller), 6x large plates
1. 9 more Large plates
