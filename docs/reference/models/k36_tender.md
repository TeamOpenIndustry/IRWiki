---
title: K36 Tender
---

# [Models](./) - K36 Tender

<ModelViewer type="tender" model-name="k36_tender" gauge="1"/>

## History/Overview

This tender pairs with the [K36](k36).

## Building

1. Frame
