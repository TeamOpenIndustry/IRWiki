---
title: Handcart
---

# [Models](./) - Handcart

<ModelViewer type="handcar"
   base-url="/IRWiki/models/rolling_stock/locomotives/handcart_1/"
   model-name="handcart_1"/>

## Overview

A simple handcart for your manual powered needs.

<WheelConfiguration>2-2-0</WheelConfiguration>

## Building

1. Frame
