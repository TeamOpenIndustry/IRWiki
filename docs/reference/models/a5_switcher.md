---
title: A5 Switcher
---

# [Models](./) - A5 Switcher

<ModelViewer type="steam" model-name="a5_switcher"/>

## Overview

[Tender](a5_tender)

<WheelConfiguration>0-4-0</WheelConfiguration>

## Building

1. Frame
