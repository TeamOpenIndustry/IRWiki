---
title: Skookum
---

# [Models](./) - Skookum

<ModelViewer type="steam" model-name="skookum"/>

## History/Overview

[Tender](skookum_tender)

<WheelConfiguration>2-4-4-2</WheelConfiguration>

## Building

1. Frame
