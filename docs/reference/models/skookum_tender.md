---
title: Skookum Tender
---

# [Models](./) - Skookum Tender

<ModelViewer type="tender" model-name="skookum_tender"/>

## Overview

This tender pairs with the [Skookum](skookum).

## Building

1. Frame
