---
title: GE 40-ton Boxcab
---

# [Models](./) - GE 40-ton Boxcab

<ModelViewer type="diesel" model-name="ge_40_ton_boxcab" gauge="0.9144"/>

## Overview

<WheelConfiguration>B-B</WheelConfiguration>

## Building

1. Frame
