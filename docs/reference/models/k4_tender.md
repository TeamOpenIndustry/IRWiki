---
title: K4 Tender
---

# [Models](./) - K4 Pacific Tender

<ModelViewer type="tender" model-name="k4_tender"/>

## Overview

This tender pairs with the [K4 "Pacific"](k4_pacific).

## Building

1. Frame
