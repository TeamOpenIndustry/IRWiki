---
title: GE B40-8W
---

# [Models](./) - GE B40-8 (Dash 8) Wide Cab

<ModelViewer type="diesel" model-name="ge_b40_8"/>

## Overview

This is a variant of the GE B40-8 with a full width cab.

<WheelConfiguration>B-B</WheelConfiguration>

## Building

1. Frame
