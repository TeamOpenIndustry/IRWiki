---
title: Cooke Tender
---

# [Models](./) - Cooke Tender

<ModelViewer type="tender" model-name="cooke_tender" gauge="1"/>

## Overview

This tender pairs with the [Cooke Mogul](cooke_mogul).

Someone else knows more about this.

## Building

1. Frame
