---
title: Immersive Railroading Reference
---

# Immersive Railroading Reference

## Items, Structures, and Entities

* [Tools](tools/)
* [Items](items/)
* [Machines](machines/)
* [Locomotives and Rolling Stock](models/)

## Mechanics

* [Train Mechanics](trains/)

## Issues

[Known Compatibility Issues](issues.md)

Any new issues should be reported via a issue request on GitLab/GitHub.
