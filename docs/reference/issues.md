---
title: Known Issues
---

# Known Issues

## Mod Conflicts

There's none here at the moment.

### Bungee/Waterfall

You will need to change a Waterfall config option relating to entity IDs across servers.  See https://github.com/cam72cam/ImmersiveRailroading/issues/579 and https://github.com/PaperMC/Waterfall/issues/136 for more details.

Please check out these config values

This is the config mentioned in the issues above: `disable_entity_metadata_rewrite=true` 
This was needed for sponge but may affect how IR works: `ip-forwarding=false`

### ViveCraft

Since ViveCraft messes with the Minecraft graphics internals, a lot of the functionality is known to break in unexpected ways.
