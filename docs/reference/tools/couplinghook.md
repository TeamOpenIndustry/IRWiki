---
title: Coupling Hook
---

This item is used check the coupling status of stock and set the coupling mode.  It is crafted as follows:

<CraftingGrid :items="['steelingot','steelingot',null,'steelingot',null,null,'steelingot',null,null]" result="couplinghook"></CraftingGrid>

## Usage

Right clicking on a piece of rolling stock near a coupler will tell you where it is coupled to and the current coupling mode.

![Couple Status](/img/couplinghook-usage1.png)

Shift + right clicking on a piece of rolling stock near a coupler will set the coupler mode.

* _Shunting mode_ means that the cars can bump into each other, but they won't couple.

* _Normal mode_ means that when cars bump into each other they will couple and form a train.