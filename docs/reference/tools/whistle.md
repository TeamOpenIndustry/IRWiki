---
title: Conductor's Whistle
---

# [Tools](./) - Conductor's Whistle

The Conductor's whistle is used to make villagers mount the nearest train to the player.
It affects villagers in a 50 block radius (configurable).

## Crafting

<CraftingGrid :items="['goldingot','goldingot',null,'goldingot','goldingot',null,'goldingot','goldingot',null]" result="whistle"></CraftingGrid>

## Usage

Right clicking with the whistle causes the villagers to board passenger cars (up to the max number allowed in the car).

![Mounted](/img/whistle-mounted.png)

Shift + right clicking with the whistle causes the villagers to disembark from the passenger cars.

![Unmounted](/img/whistle-unmounted.png)

Villagers will pay you 1 emerald for each KM traveled (1000 blocks).  
This is calculated as the crow flies, if you just take them in a circle there will be no reward.
