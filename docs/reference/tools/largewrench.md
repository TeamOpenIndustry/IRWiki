---
title: Large Wrench
---

# [Tools](./) - Large Wrench

The large wrench is the equivilent of the Engineer's Hammer in IR.

## Crafting

<CraftingGrid :items="[null,'steelingot',null,'steelingot','steelingot','steelingot','steelingot',null,'steelingot']" result="largewrench"></CraftingGrid>

## Usage

Form Multiblock Machines by right-clicking on them after all blocks are placed.

Assemble locomotives/rolling stock by right-clicking with the required part in inventory.

Disassemble locomotives/rolling stock by shift + right-clicking.
