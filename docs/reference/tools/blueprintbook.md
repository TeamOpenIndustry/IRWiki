---
title: Blueprint Book
---

# [Tools](./) - Blueprint Book

The Blueprint Book is one of the core tools in Immersive Railroading.
It is similar to the Immersive Engineering projectors.
However, instead of having separate projectors, the Blueprint Book will produce a ghost of any [machines](/reference/machines/) in IR.
Also, instead of placing a ghost image, the Blueprint Book is used to place the blocks needed.

The Blueprint Book also opens up an in-game wiki if the in-game wiki mod is installed.
Otherwise, it links to the wiki on GitHub.

## Crafting

Craft with a book and six steel ingots in an H-shape, with the book in the center.

<CraftingGrid :items="['steelingot',null,'steelingot','steelingot','book','steelingot','steelingot',null,'steelingot']" result="blueprintbook"></CraftingGrid>

## Usage

While holding the book with your crosshair in midair, sneak and right-click to toggle between the different machines.
See the [Machines Reference](/reference/machines/) to see the different machines in IR.

To place a machine, right-click on the ground with the Blueprint Book in hand. 
The Blueprint Book will display if you are missing the blocks required, and will also alert you to the coordinates of any blocks which must be removed to place the chosen machine.

![Preview](/img/blueprintbook-usage1.png)
*Preview shown when looking at the ground with the book*
