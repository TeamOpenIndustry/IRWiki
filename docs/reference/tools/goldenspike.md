---
title: Golden Spike
---

# [Tools](./) - Golden Spke

The Golden Spike is used to configure flex track.

<CraftingGrid :items="['goldingot','goldingot',null,'goldingot',null,null,'goldingot',null,null]" result="goldenspike"></CraftingGrid>

## Usage

1. Place a [track blueprint](trackblueprint.md) while in _Custom Curve Mode_
1. Right-Click on the placed blueprint with the golden spike
1. Go to the end point of the track and face where the track is coming from
1. Right-Click on the ground. This is similar to placing a track blueprint. A new ghost will appear.
1. To complete the track, Shift-Right Click the blueprint with your hand.

<figure class="video_container">
  <iframe src="https://giant.gfycat.com/ShinyCompetentBarnowl.webm" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

See the [Custom Curves Guide](/guides/curvedtrack.md) for detailed usage information.
