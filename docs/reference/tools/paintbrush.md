---
title: Paintbrush
---

# [Tools](./) - Paintbrush

It paints stuff...

## Crafting

<CraftingGrid :items="[null,'wool',null,null,'ironingot',null,null,'stick',null]" result="paintbrush"></CraftingGrid>

## Usage

Right-click locomotives and stock to switch colors.

![Green A1 Peppercorn](/img/peppercorn.png)

![Blue A1 Peppercorn](/img/peppercorn-blue.png)
