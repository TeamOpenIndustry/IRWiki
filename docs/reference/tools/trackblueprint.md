---
title: Track Blueprint
---

# [Items](./) - Track Blueprint

This tool is used to lay down rails for rolling stock to travel on. It is crafted with a piece of paper and six Steel Ingots in an H shape, like so:

<!-- ![Track Blueprint crafting recipe](/img/trackblueprint-crafting.png) -->

<CraftingGrid :items="['steelingot',null,'steelingot','steelingot','paper','steelingot','steelingot',null,'steelingot']" result="trackblueprint"></CraftingGrid>

With the blueprint in hand, right-click in midair to change its settings. Right-click a block to place the Rail Bed, which requires Track Segments from the [Track Roller](/reference/machines/trackroller.md) and Treated Wood Planks (Normal planks if IE is not installed).

## In game UI

![Track Blueprint GUI](/img/trackblueprint-gui.png)

## Settings

### Type

This setting determines what shape your track will take. 
Current options are Straight, Slope, Turn\*, Switch, Crossing\*, Turntable, and Custom Curve.

::: warning
The Crossing and Turn types are obsolete as of version 1.5.0.
Tracks can now cross other tracks of any gauge and at any angle.
The new Custom Curve type, which is far less finicky, more flexible, and much easier to use, has rendered Turn completely useless.
Crossing and Turn remain in the mod purely for backwards compatibility.
See the [Custom Curves Guide](/guides/curvedtrack.md) for more info on them.
:::

### Length

This setting determines how many blocks long the piece of track is.
For the Custom Curve, this number is irrelevant.

### Turn Degrees

This setting shows up only for Turn and Switch types.  
It determines how large/long the turn's angle should be.
Angles are in 22.5&deg; increments (slice a pie into 16 slices).

![Angles](/img/angles.png)

### Rail Bed

This setting determines what visual rail bed should be placed between the rail ties.
This option is useful for debugging when you want to see where the track blocks will actually be placed.
Entries are pulled from the railBed ore dictionary and can be added to.

![Brick Rail Bed](/img/trackblueprint-brickbed.png)

### Rail Bed Fill

This setting is useful for building bridges.
It auto-places blocks from your inventory under the rail bed as it builds the tracks.
It uses the same ore dictionary as the Rail Bed.

The image below shows the fill using yellow concrete.

![Bed Fill](/img/trackblueprint-yellowfill.png)

### Position

This setting determines how the track should be locked to the rail bed.

* Fixed: Locks the track directly to the rail bed, no flexibility at all
* Pixels: Allows free placement of the track (rounded to the nearest 16th of a block)
* Pixels Locked: Allows free placement of the track forward and backward, but locks side to side motion
* Smooth: Allows free placement of the track
* Smooth Locked: Allows free placement of the track forward and backward, but locks side to side motion

### Gauge

This setting determines what gauge the track should be built at.
Currently, there are 5 sizes.

* Model (smallest)
* Minecraft (1 Meter Wide)
* Narrow
* Standard
* Brunnel

### Grade Crossing

Extends the Rail Bed out to the sides, to have the appearance of a level crossing.
This is similar to intersections of roads and railroads tracks in real life.

### Place Blueprint

You can place down a blueprint which renders the track in-world.
This allows you to plan, move and shape the track as you go.

![Placed Track Blueprint](/img/trackblueprint-placed.png)

Right clicking on a placed blueprint will allow you to change the settings without having to replace it.

Shift + right clicking on a placed blueprint will allow you to shift it's position if it is in any mode except Fixed.

Breaking the block will remove the blueprint.
Shift + breaking the block will attempt to place the blueprint.
