---
title: Boiler Plate
---

# [Items](./) - Boiler Plate

The boilerplate looks similar to the normal plate but is used in the making of steam boilers.

## Crafting

The Boiler Plate is made in the [Casting Basin](/reference/machines/castingbasin.md).

## Usage

The boiler plate is used while building a steam locomotive.
Please see the Getting's started guide section on [Building your first Locomotive](/guides/gettingstarted/firstlocomotive.md)
