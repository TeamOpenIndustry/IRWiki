---
title: Rail Castings
---

# [Items](./) - Rail Castings

## Creation

Rail castings are made with the [Casting Basin](/reference/machines/castingbasin.md).

## Usage

Rail castings are used with the [Track Roller](/reference/machines/trackroller.md) to make tracks.
