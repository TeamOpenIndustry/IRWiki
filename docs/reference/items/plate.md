---
title: Plate
---

# [Items](./) - Plates

There are 3 sizes of plates - small, medium, and large.

All three are made on the [Plate Roller](/reference/machines/plateroller.md).

## Crafting

how.

## Usage

Plates are used on frames to build locomotives and rolling stock.
Please see [Getting Started - First Locomotive](/guides/gettingstarted/firstlocomotive.md) on how to build these.
