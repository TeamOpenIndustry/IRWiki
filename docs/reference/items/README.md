---
title: Items
---

# Immersive Railroading Items

* [Rail Casting](railcasting.md)
* [Boiler Plate](boilerplate.md)
* [Plate](plate.md)
* Train Components are made with the [Casting Basin](/reference/machines/castingbasin.md)

> How to know when to use the steam hammer for post-processing?
