---
title: Train Radio Control Card
---

# [Items](./) - Train Radio Control Card

This item is only available if you have OpenComputers.

The Train Radio Control Card is used for controlling a train remotely with a computer.

## Crafting

<CraftingGrid :items="[null,'ironbars',null,'transistor','ironingot','transistor',null,'datacard',null]" result="radiocard"></CraftingGrid>

## Usage

The Card is placed in the computer like any other card.

> Need an example Image

Please see the [OpenComputer Guide](/guides/opencomputers/) on how to program using OpenComputers.
