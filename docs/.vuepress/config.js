module.exports = {
    title: 'Immersive Railroading',
    base: '/IRWiki/',
    description: 'Realistic Rail-based transport in Minecraft',
    // base: '',
    dest: 'public',
    themeConfig: {
        nav: [
            { text: 'Download', link: 'https://minecraft.curseforge.com/projects/immersive-railroading'},
            { text: 'Guides', link: '/guides/'},
            { text: 'Reference', link: '/reference/'},
            { text: 'Development', link: '/developers/'}
        ]
    },
    plugins: [
      [
        'vuepress-plugin-mathjax',
        {
          target: 'svg',
          macros: {
            '*': '\\times',
          },
        },
      ],
    ],
    head: [['script', {}, `
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="https://analytics.blackhawkelectronics.com/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '6']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
    `]]
}
