const VueGL = require('vue-gl');
import ToggleButton from 'vue-js-toggle-button/dist/ssr.index';

export default ({
    Vue, // the version of Vue being used in the VuePress app
    options, // the options for the root Vue instance
    router, // the router instance for the app
    siteData, // site metadata
    isServer // is this enhancement applied in server-rendering or client
}) => {
    Object.keys(VueGL).forEach((name) => Vue.component(name, VueGL[name]));
    Vue.use(ToggleButton);
}