import { Object3D, Geometry, ParticleBasicMaterial, Vector3, ParticleSystem } from 'three';

import { VglObject3d } from 'vue-gl';

import { OBJLoader2 } from 'three/examples/jsm/loaders/OBJLoader2.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from 'three/examples/jsm/loaders/obj2/bridge/MtlObjBridge';


export default {
  inject: ['vglNamespace'],
  mixins: [VglObject3d],
  props: ['visible'],
  data() {
    return {
      baseObject: new Object3D()
    };
  },
  computed: {
    inst() {      
      var objLoader = new OBJLoader2();
      var mtlLoader = new MTLLoader();

      mtlLoader.setPath('/IRWiki/models/track/default/');

      this.$emit('loading');

      var self = this;

      mtlLoader.load(
          "track_1m_model.mtl", 
          function(materials) { // onLoad of Material
              materials.preload();
              const procMaterials = MtlObjBridge.addMaterialsFromMtlLoader(materials);
              objLoader.setBaseObject3d(self.baseObject);
              objLoader.addMaterials(procMaterials);
              objLoader.load(
                  "/IRWiki/models/track/default/track_1m_model.obj", 
                  function(loadedObj) { // onLoad of Object
                    self.vglObject3d.emit();
                    self.$emit('loaded');
                  }, 
                  (xhr) => { 
                    self.$emit('loading',xhr.loaded/xhr.total);
                  },
                  err => {
                      console.error(err)
                  }
              )
          },
          () => {}, // onProgress
          err => { // onError
              console.error(err)
          }
      );

      return this.baseObject;
    },
  }
};