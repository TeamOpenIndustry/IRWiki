import { Object3D, Geometry, ParticleBasicMaterial, Vector3, ParticleSystem } from 'three';
import * as THREE from 'three';

import { VglObject3d } from 'vue-gl';

import { OBJLoader2 } from 'three/examples/jsm/loaders/OBJLoader2.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from 'three/examples/jsm/loaders/obj2/bridge/MtlObjBridge';

function checkVisibility(newVis, name){
  // Always hidden objects
  if(name.match(/(PRESSURE_VALVE_|CHIMNEY_|CHIMINEY_|EXHAUST_|RAIL_BASE|FINISHED_PREVIEW).*/)){
    return false;
  }

  // If new visibility is undefined or all, then show everything.
  if(newVis == undefined || newVis == '' || newVis == 'all'){
    return true;
  } 
  /*
    Item Names from src\main\java\cam72cam\immersiverailroading\library\RenderComponentType.java
    Locomotive Only: CAB, BELL, WHISTLE, HORN
    Frame: FRAME (!FRAME_WHEEL)
    Wheels: BOGEY... FRAME_WHEEL
    Diesel: FUEL_TANK, ALTERNATOR, ENGINE_BLOCK, CRANKSHAFT, GEARBOX, PISTON, FAN, DRIVE_SHAFT, FLUID_COUPLING, FINAL_DRIVE, TORQUE_CONVERTER
    Steam: FIREBOX, SMOKEBOX, STEAM_CHEST, BOILER, PIPING
    Steam Walchert valves: WHEEL_DRIVER, CYLINDER_SIDE, SIDE_ROD, MAIN_ROD, PISTON_ROD, UNION_LINK, COMBINATION_LEVER, VALVE_STEM, RADIUS_BAR, EXPANSION_LINK, ECCENTRIC_ROD, ECCENTRIC_CRANK, REVERSING_ARM, LIFTING_LINK, REACH_ROD
    Steam Mallet: FRONT_LOCOMOTIVE
    Cargo: CARGO_FILL
    Everything else
    This visibility function is similar to src\main\java\cam72cam\immersiverailroading\registry\EntityRollingStockDefinition.java addComponentIfExists()
  */
  if(newVis.includes("frame") && !name.includes("WHEEL") && name.match(/FRAME|FRONT_LOCOMOTIVE/)) return true;
  if(newVis.includes("cab") && name.includes("CAB")) return true;
  if(newVis.includes("wheel") && name.match(/WHEEL|FRAME_WHEEL|WHEEL_DRIVER/)) return true;
  if(newVis.includes("bogey") && name.includes("BOGEY")) return true;
  if(newVis.includes("engine") && name.match(/ALTERNATOR|ENGINE_BLOCK|CRANKSHAFT|GEARBOX|PISTON/)) return true;
  if(newVis.includes("chest") && name.includes("STEAM_CHEST")) return true;
  if(newVis.includes("valves") && name.match(/CYLINDER_SIDE|SIDE_ROD|MAIN_ROD|PISTON_ROD|UNION_LINK|COMBINATION_LEVER|VALVE_STEM|RADIUS_BAR|EXPANSION_LINK|ECCENTRIC_ROD|ECCENTRIC_CRANK|REVERSING_ARM|LIFTING_LINK|REACH_ROD/)) return true;
  if(newVis.includes("boiler") && name.match(/FIREBOX|SMOKEBOX|BOILER/)) return true;
    
  return false;
}

export default {
  inject: ['vglNamespace'],
  mixins: [VglObject3d],
  props: ['baseUrl','modelName','visible'],
  data() {
    return {
      baseObject: new Object3D(),
      curObject: new Object3D()
    };
  },
  computed: {
    inst() {      
      var objLoader = new OBJLoader2();
      var mtlLoader = new MTLLoader();

      mtlLoader.setPath(this.baseUrl);

      this.$emit('loading');

      var self = this;

      mtlLoader.load(
          this.modelName + ".mtl", 
          function(materials) { // onLoad of Material
              materials.preload();
              const procMaterials = MtlObjBridge.addMaterialsFromMtlLoader(materials);
              objLoader.setBaseObject3d(self.baseObject);
              objLoader.addMaterials(procMaterials);
              objLoader.setUseOAsMesh(true);
              objLoader.load(
                  self.baseUrl + self.modelName + ".obj", 
                  function(loadedObj) { // onLoad of Object
                    self.curObject = loadedObj;
                    self.visibleWatcher("all"); // this will trigger emit
                    self.$emit('loaded');
                  }, 
                  (xhr) => { 
                    self.$emit('loading',xhr.loaded/xhr.total);
                  },
                  err => {
                      console.error(err)
                  }
              )
          },
          () => {}, // onProgress
          err => { // onError
              console.error(err)
          }
      );

      return this.baseObject;
    },
  },
  methods: {
    visibleWatcher(newVis){
      if(typeof newVis == "string") {
        newVis = newVis.toLowerCase();
        newVis = newVis.split('|');
      }
      for(let i = this.baseObject.children.length - 1; i >= 0; i--){
        this.baseObject.children[i].visible = checkVisibility(newVis, this.baseObject.children[i].name);
      }
      this.vglObject3d.emit();
    }
  },
  watch: {
    visible: function(newVis){
      this.visibleWatcher(newVis)
    }
  }
};