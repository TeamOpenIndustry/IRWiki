import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { VglObject3d } from 'vue-gl';
import {Vector2 } from 'three';

export default {
  inject: ['vglNamespace'],
  mixins: [VglObject3d],
  props: [ "camera"],
  computed: {
    cmr () {
      return this.vglNamespace.cameras.hash[this.camera];
    }
  },
  watch: {
    cmr: {
      handler(camera) {
        const domElement = this.$parent.$parent.inst.domElement;
        const controls = new OrbitControls(camera,domElement)
        controls.addEventListener('change', () => {
            this.vglObject3d.emit();
        });
        

        var centerPosition = controls.target.clone();
        centerPosition.y = 0;
        var groundPosition = camera.position.clone();
        groundPosition.y = 0;
        var d = (centerPosition.distanceTo(groundPosition));

        var origin = new Vector2(controls.target.y,0);
        var remote = new Vector2(0.1,d); // replace 0 with raycasted ground altitude
        var angleRadians = Math.atan2(remote.y - origin.y, remote.x - origin.x);
        controls.maxPolarAngle = angleRadians;
      },
      immediate: true,
    }
  },
  render(h) {
    return h('div');
  }
}
