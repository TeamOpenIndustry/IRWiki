import { CubeTextureLoader } from 'three';
export default {
    inject: {
        vglNamespace: {
            default() { throw new Error('VueGL components must be wraped by VglNamespace component.'); },
        }
    },
    props: ['name'],
    computed: {
        inst(){
            const loader = new CubeTextureLoader();
            return loader.load([
                '/IRWiki/img/skybox/Side.png',
                '/IRWiki/img/skybox/Sun.png',
                '/IRWiki/img/skybox/Up.png',
                '/IRWiki/img/skybox/Down.png',
                '/IRWiki/img/skybox/Side.png',
                '/IRWiki/img/skybox/Side.png',
            ],(texture) => { 
                this.update(); 
            });
        }
    },
    methods: {
        /** Emit an event in the `textures` namespace. */
        update() {
            if (this.name !== undefined) this.vglNamespace.textures.emit(this.name, this.inst);
        },
    },
    beforeDestroy() {
        if (this.name !== undefined) this.vglNamespace.textures.delete(this.name, this.inst);
    },
    watch: {
        inst: {
            handler(inst) {
                if (this.name !== undefined) {
                    this.vglNamespace.textures.set(this.name, inst);
                    this.$parent.setBackgroundTexture(this.name);
                }
            },
            immediate: true,
        },
        name(newName, oldName) {
            if (oldName !== undefined) this.vglNamespace.textures.delete(oldName, this.inst);
            if (newName !== undefined) this.vglNamespace.textures.set(newName, this.inst);
        },
    },
    render(h) {
      if (!this.$slots.default) return undefined;
      return h('div', { style: { display: 'none' } }, this.$slots.default);
    },
}