---
home: true
heroImage: /img/IR_Home_Banner.png
features:
- title: Getting Started
  details: Don't know where to start? Check out the Getting Started Guide in the Guides section.
  link: /guides/gettingstarted/
- title: Reference
  details: All the features of Immersive Railroading.
  link: /reference/
- title: Developers
  details: Want to contribute? Head over to this section
  link: /developers/
footer: This project is LGPL 2.1 Licensed | Copyright ©2020 Immersive Railroading Dev Team
---

Please note that this site is in _testing_ and is not yet the official site. 
Please see our [wiki](https://github.com/cam72cam/ImmersiveRailroading/wiki) instead.

[![Downloads](http://cf.way2muchnoise.eu/full_immersive-railroading_downloads.svg)](https://minecraft.curseforge.com/projects/immersive-railroading)

<small>Railroad font from [here](https://www.dafont.com/railroad-roman-1916.font)</small>
