---
title: Guides
---

[Getting Started](gettingstarted/)

[Curved Tracks](curvedtrack.md)

[Automating with OpenComputers](opencomputers/)
