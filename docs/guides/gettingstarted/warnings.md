---
title: Warnings
---

# [Getting Started](./) - Warnings

This is a list of pitfalls for new players.

* Not enough water in a steam engine, leading to explosion
* Too steep of inclines. Remember: 1 up, 10 over is the maximum incline. Otherwise you won't be able to control the train.
* Using up your steel in survival before getting a good enough steel production system.
