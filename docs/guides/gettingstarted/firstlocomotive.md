---
title: Getting Started - Your first Locomotive
---

Making a locomotive or any rolling stock is easy in Immersive Railroading.
(Gathering all the materials may not be)

## A1 Peppercorn Video

Hopefully, there will be a video of how to make the [A1 Peppercorn Steam Engine](../../reference/models/a1_peppercorn.ht) here some day.

## Gathering the parts and putting it together

The process of gathering the parts and putting together a locomotive, or any other stock, is done at the same time.

First, make a frame for the locomotive of your choice in the [Casting Basin](/reference/machines/castingbasin.md).

Next, place this frame on some rails by right-clicking with it on the tracks.
If you are standing to the side of the rails, the frame will be placed to the left where you are facing.
If you are facing down the tracks, it will be placed in the direction you are facing.

Once the frame is placed, you need to take your [Large Wrench](/reference/tools/largewrench.md), and right-click the frame.
You will be notified what parts you need to continue to build the locomotive.
Parts will be given in order that they are placed.

To place parts, right-click the frame, just as before, with the parts in your inventory.
They will be placed on the frame automatically, one at a time.

Most parts begin with the casting basin except plates.
Several parts will probably need post-processing in the [Steam Hammer](/reference/machines/steamhammer.md).
Plates must be flattened using the [Plate Roller](/reference/machines/plateroller.md).
Boiler plates for steam locomotives must be rolled using the [Boiler Roller](/reference/machines/boiler.md).

## Steaming it up

For steam locomotives, you will need plenty of water and solid fuel.

For Diesel locomotives, you will need plenty of liquid fuel - bio-diesel or regular diesel if you have Immersive Petroleum.

Both types have to warm up before they can be used.

Diesel locomotives, are turned on by pressing __+__ on the numpad by default.

::: danger WARNING!
If you run below 75% on water on a steam locomotive, you will damage your boiler, resulting in an explosion that destroys all your hard work.
:::

## Go for a Ride

Once your locomotive has warmed up, you are ready to go!

By default, the controls are all on the number pad:

* Throttle Up and Down: __8__ and __2__, __5__ centers
* Brake Up and Down: __7__ and __1__, __4__ turns off
* Deadman's Switch: __=__
* Horn: __Enter__
* Bell: __-__

The throttle is currently directional.
The middle is neutral, the top is forwards, and the bottom is reverse.
