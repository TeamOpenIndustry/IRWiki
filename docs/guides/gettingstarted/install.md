---
title: How to Install
---

# [Getting Started](./) - Installing Immersive Railroading

> Please do not follow these instructions until this is the official site!

## Hardware Requirements

It is best to have 8GB of memory in your computer to be able to use IR.
Minecraft should be set to using 4GB of memory.

## Dependencies

Immersive Railroading requires the following mods:

  - [TrackAPI](https://www.curseforge.com/minecraft/mc-mods/track-api)

The following mods are highly recommended for the full immersive experience.

  - [Immersive Engineering](https://www.curseforge.com/minecraft/mc-mods/immersive-engineering) (Needed for steel production)
  - [Immersive Petroleum](https://www.curseforge.com/minecraft/mc-mods/immersive-petroleum) (Needed for diesel if you don't want to make biodiesel)

## Downloading

Some launchers will be able to download and install IR, such as ATLauncher.
If you are not using a launcher that integrates with the curseforge catalog, you will have to install manually.

Download the latest release from Curseforge. Click on the download link at the top of the page.

For the dependencies, use the links above.

Place all files in the mods folder like normal.

## Resource Packs

Immersive Railroading comes with a few models.
The list of [Resource packs](/reference/models/resourcepacks.md) is kept on its own page.

To install, drop the zip files into IR's config folder (.minecraft/config/immersiverailroading).
