---
title: Getting Started - Making Tracks
---

# [Getting Started](./) - Making Tracks

1. Craft a [Track Blueprint](/reference/tools/trackblueprint.md).
1. Have the following in your inventory:
   * Your support material, if used
   * Your filler material, if used
   * Rails
   * Your tie material
1. Right click with the track blueprint in the air. Select your settings and close. If you are doing a custom curve, make sure to check _Place Blueprint_.
1. Right-click on the starting block, facing the direction you want the track to go. A ghost will appear as you do this.
1. If you are doing a custom curve, you will need a [Golden Spike](/reference/tools/goldenspike.md). Please see the [Curved Track Guide](/guide/curvedtrack.md).
1. Left-click the blueprint origin with your hand to complete the track if you have _Place Blueprint_ turned on.

Tracks are useless without something on them. The next thing is to [make your first locomotive](firstlocomotive.md).