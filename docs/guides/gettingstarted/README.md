---
title: Getting Started
---

# Getting Started

> This is the original wiki page. A more in-depth guide is coming.

Before establishing a railroad empire, it's a good idea to ensure a robust supply of Steel, Treated Wood, and electrical power.

When ready, create a [Blueprint Book](/reference/items/blueprintbook.md), a [Large Wrench](/reference/items/largewrench.md), and a [Track Blueprint](/reference/items/trackblueprint.md). These three tools are essential to progress and construction through the mod.

Setting up a clear, level space at least 2 chunks by 2 chunks is recommended.  This will provide enough space for constructing machinery and rolling stock. The first machines you will need are the [Casting Basin](/reference/machines/castingbasin.md) and [Track Roller](/reference/machines/trackroller.md) to create tracks. Making tracks requires a Rail Casting, which is made in the Casting Basin.

Next, place down at least 2 lengths of track end-to-end to have enough space to place a Frame on the rails, which is created in the Casting Basin. Make sure your Frame and Track gauges match up! With the Frame in hand, right-click the rail to place it. Then, with the Large Wrench in hand, right-click the Frame. It will list the parts needed to complete the piece of stock and which machines are needed in order to make those parts. For example, a newly placed A1 Peppercorn Frame will list out that it needs 2 Cylinders, which are made in the Casting Basin and then processed in the [Steam Hammer](/reference/machines/steamhammer.md).

Continuing on with the creation of an A1 Peppercorn, we don't need another machine until we get to the point of creating the engine's firebox and boiler, using the [Boiler Roller](/reference/machines/boilerroller.md) and [Plate Rolling Machine](/reference/machines/plateroller.md).

A word of warning: Steam Engines require a constant water supply when fueled, and if they boil off 75% or more of their water, there is a chance that they will explode. As such, it's a good idea to remove any fuel from idle Steam Engines, or ensure they have a constant supply of water through a Loader and fluid piping.

Locomotives and cars can be instantly removed and picked up by using sneak + click on them.

## New Pages

1. [Installing Immersive Railroading](install.md)
1. [Warnings](warnings.md)
1. [Making Tracks](makingtracks.md)
1. [Your First Locomotive](firstlocomotive.md)
